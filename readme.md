# Dummy API

## Installation for production

Add `DUMMY_API_SECRET`, `METAPLATTFORM_URL`, `METAPLATTFORM_CLIENT_URL`, `METAPLATTFORM_CLIENT_ID`, `METAPLATTFORM_CLIENT_SECRET` to the `.env` (the BearerToken for the API)

Add a new node `repositories` to your `composer.json`:
```
"repositories": [
   {
     "type": "vcs",
     "url": "https://bitbucket.org/hellgrau/ifc-crm-dummy"
   }
]
```

Add the repository to your `require` section subsequently:
```
"require": {
    "hellgrau/ifc-crm-dummy": "*"
}
```

Run `composer install`

## Installation for development

Add `DUMMY_API_SECRET` to the `.env` (the BearerToken for the API)

* In your Laravel project document root create a new directory `packages/hellgrau/`
* Within the new dir run `git clone git@bitbucket.org:hellgrau/ifc-crm-dummy.git`
* Add `\Hellgrau\DummyApi\ServiceProvider::class` to your `config/app.php`
* Run `composer install`
* Add the autoloader to your `composer.json`:
```
"autoload": {
    "psr-4": {
        "Hellgrau\\DummyApi\\": "packages/hellgrau/ifc-crm-dummy/"
    }
},
```
