<?php

namespace Hellgrau\DummyApi\Factories;

/** @var Factory $factory */

use Faker\Generator as Faker;
use Hellgrau\DummyApi\Models\CustomerSecurity;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CustomerSecurity::class, function (Faker $faker) {
    $type = ['OTHER', 'IMMOBILIE'];


    return [
        'customer_id' => $faker->numberBetween(0, 50),
        'title' => $faker->name,
        'type' => $faker->randomElement($type),
        'amount' => $faker->numberBetween(111, 10000),
        'certification_date' => $faker->dateTimeBetween('-2 years', 'now'),
    ];
});
