<?php

namespace Hellgrau\DummyApi\Factories;

/** @var Factory $factory */

use Faker\Generator as Faker;
use Hellgrau\DummyApi\Models\CustomerFinancialAsset;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CustomerFinancialAsset::class, function (Faker $faker) {
    $type = ['SOLVENCY', 'EXPENDITURE', 'LIABILITY', 'WEALTH'];
    $interval = ['MONTHLY', 'YEARLY', 'ONCE'];

    return [
        'customer_id' => $faker->numberBetween(0, 50),
        'title' => $faker->name,
        'creditor' => $faker->firstName . ' ' . $faker->lastName,
        'type' => $faker->randomElement($type),
        'interval' => $faker->randomElement($interval),
        'interval_amount' => $faker->numberBetween(1, 1000),
        'start_amount' => $faker->numberBetween(1, 1000),
        'start_date' => $faker->dateTimeBetween('-2 years', 'now'),
        'end_date' => $faker->dateTimeBetween('1 months', '10 years'),
    ];
});
