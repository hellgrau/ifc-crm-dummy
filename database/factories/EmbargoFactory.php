<?php

namespace Hellgrau\DummyApi\Factories;

/** @var Factory $factory */

use Faker\Generator as Faker;
use Hellgrau\DummyApi\Models\Embargo;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Embargo::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'birthday' => $faker->date(),
        'nationality' => $faker->randomElement(['CH', 'IR', 'GB']),
    ];
});
