<?php

namespace Hellgrau\DummyApi\Factories;

/** @var Factory $factory */

use Faker\Generator as Faker;
use Hellgrau\DummyApi\Models\Customer;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Customer::class, function (Faker $faker) {
    $salutations = ['Sehr geehrter Herr', 'Sehr geehrte Frau'];
    $genders = ['m', 'w'];

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'birthday' => $faker->date(),
        'salutation' => $salutations[array_rand($salutations)],
        'gender' => $genders[array_rand($genders)],
        'embargo' => $faker->boolean(10),
        'iban' => $faker->iban('DE'), // password
        'bic' => $faker->swiftBicNumber, // password
        'schufa_score' => $faker->numberBetween(0, 99.9),
        'is_applicant' => $faker->boolean(50),
        'post_code' => $faker->numberBetween(10000, 99999),
        'city' => $faker->city,
        'street' => $faker->streetName,
        'house_number' => $faker->numberBetween(0, 99),
    ];
});
