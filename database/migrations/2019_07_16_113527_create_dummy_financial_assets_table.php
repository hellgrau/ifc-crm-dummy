<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDummyFinancialAssetsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('dummy_customer_financial_assets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id');
            $table->string('title')->nullable();
            $table->string('creditor')->nullable();
            $table->enum('type', ['SOLVENCY', 'EXPENDITURE', 'LIABILITY', 'WEALTH'])->default('SOLVENCY');
            $table->enum('interval', ['MONTHLY', 'YEARLY', 'ONCE'])->default('MONTHLY');
            $table->float('interval_amount')->nullable();
            $table->float('start_amount')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dummy_customer_financial_assets');
    }
}
