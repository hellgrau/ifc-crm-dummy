<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDummySecuritysTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('dummy_customer_securities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id');
            $table->string('title')->nullable();
            $table->enum('type', ['WERTTPAPIERE', 'IMMOBILIE', 'BUERGSCHAFT', 'GUETER'])->default('IMMOBILIE');
            $table->float('amount')->nullable();
            $table->dateTime('certification_date')->default(NOW());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dummy_customer_securities');
    }
}
