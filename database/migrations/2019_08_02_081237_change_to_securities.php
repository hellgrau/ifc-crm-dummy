<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeToSecurities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('dummy_customer_securities');
        Schema::create('dummy_customer_securities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id');
            $table->string('title')->nullable();
            $table->enum('type', ['OTHER', 'ESTATE'])->default('ESTATE');
            $table->string('subtype')->nullable();
            $table->float('amount')->nullable();
            $table->dateTime('certification_date')->default(NOW());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
