<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDummyDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('dummy_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->nullable();
            $table->integer('document_type_id')->nullable();
            $table->string('mime')->default('');
            $table->string('name')->default('');
            $table->integer('size');
            $table->text('path');
            $table->integer('sort')->nullable();
            $table->integer('mandator_id')->nullable();
            $table->softDeletes();
            $table->integer('category_id')->nullable();
            $table->integer('offer_id')->nullable();
            $table->integer('user_id')->nullable()->index('documents_user_id');
            $table->integer('template_id')->nullable()->index('documents_template_id');
            $table->timestamps();
            $table->index(['id', 'name', 'mime', 'size', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dummy_documents');
    }
}
