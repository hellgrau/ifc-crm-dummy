<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDummyCustomersTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('dummy_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('salutation');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender');
            $table->date('birthday');
            $table->string('iban');
            $table->string('bic');
            $table->string('schufa_score');
            $table->boolean('is_applicant');
            $table->boolean('embargo');

            // Adress
            $table->string('post_code');
            $table->string('city');
            $table->string('street');
            $table->string('house_number');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dummy_customers');
    }
}
