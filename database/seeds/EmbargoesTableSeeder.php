<?php


namespace Hellgrau\DummyApi\Seeders;

use Hellgrau\DummyApi\Models\Embargo;
use Illuminate\Database\Seeder;

class EmbargoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        factory(Embargo::class, 50)->create();
    }
}
