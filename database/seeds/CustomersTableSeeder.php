<?php


namespace Hellgrau\DummyApi\Seeders;

use Hellgrau\DummyApi\Models\Customer;
use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        factory(Customer::class, 50)->create();
    }
}
