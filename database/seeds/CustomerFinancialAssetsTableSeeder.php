<?php


namespace Hellgrau\DummyApi\Seeders;

use Hellgrau\DummyApi\Models\CustomerFinancialAsset;
use Illuminate\Database\Seeder;

class CustomerFinancialAssetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        $liabilities = [
            'Arbeitgeberdarlehen',
            'Baufinazierung',
            'Bauspardarlehen',
            'Bürgschaft für Fremddarlehen',
            'Girokonto',
            'Persönlicher Kredit',
            'Versicherungshypothek',
            'Verwandtendarlehen',
            'Öffentl. Darlehen'
        ];
        $wealth = [
            'Aktien',
            'Ansprüche aus Darlehensvertäge',
            'Ansprüche aus KV',
            'Bankguthaben',
            'Bausparvertag',
            'Cardif Hypoprotect',
            'Direktversicherung',
            'Festgeld',
            'Festverzinsliche WP',
            'Fondsanteile (mit Sparraten)',
            'Genussrechte bei Dritten',
            'Inhaberschuldverschreibung',
            'Kapital-LV',
            'Kraftfahrzeug',
            'Mitgliedschaft bei Dritten',
            'Münzen Edelm./Sonst.',
            'Nachrangverbindlichkeiten bei Dritten',
            'Rentenversicherung',
            'Restschuld-LV',
            'Riester-Rente',
            'Risiko-LV',
            'Sonst. Rechte u. Ansp.',
            'Sonstige Wertpapiere bei Dritten',
            'Sparguthaben',
            'Sparvertäge (mit Sparraten)'
        ];

        for ($i = 0; $i < 50; $i++) {
            factory(CustomerFinancialAsset::class)->create(['subtype' => $liabilities[array_rand($liabilities)], 'type' => 'LIABILITY']);
            factory(CustomerFinancialAsset::class)->create(['subtype' => $wealth[array_rand($wealth)], 'type' => 'WEALTH']);
        }
    }


}
