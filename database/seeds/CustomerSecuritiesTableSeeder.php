<?php


namespace Hellgrau\DummyApi\Seeders;

use Hellgrau\DummyApi\Models\CustomerSecurity;
use Illuminate\Database\Seeder;

class CustomerSecuritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        $other = [
            'Altenteil',
            'Auflassungsvormerkung',
            'Bergschadenverzicht',
            'Dienstbarkeit',
            'Erbbauzins',
            'Gesamtgrundschuld',
            'Gesamthypothek',
            'Grundschuld',
            'Hypothek',
            'Insollvenzvermerk',
            'Mietabtretung',
            'Negativerklärung',
            'Nießbrauch',
            'Pflegerecht',
            'Reallast',
            'Rückauflassungsvormerkung',
            'Sanierungsvermerk',
            'Testamentvollstreckervermerk'
        ];

        $estate = [
            'Bauplatz',
            'Eigentumswohnung',
            'Einfamilienhaus',
            'Zweifamlienhaus',
            'Dreifamilienhaus',
            'Mehrfachfamilienhaus',
            'Sons. Immo. Besitz',
            'Mieteinnahmen wohnwirtschaftlich',
            'Doppelhaushälfte',
            'Reiheneckhaus',
            'Reihenmittelhaus',
            'Mieteinnahmen gewerblich'
        ];

        for ($i = 0; $i < 80; $i++) {
            factory(CustomerSecurity::class)->create(['subtype' => $estate[array_rand($estate)], 'type' => 'ESTATE']);
            factory(CustomerSecurity::class)->create(['subtype' => $other[array_rand($other)], 'type' => 'OTHER']);
        }
    }
}
