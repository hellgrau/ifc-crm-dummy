@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="mb-3">
            Angemeldet als: {{ auth()->user()->name }} (<a href="{{ route('dummy-api.logout') }}">abmelden</a>)
        </div>

        <div class="form-group">
            <a href="{{ route('dummy-api.sso.token') }}" class="btn btn-primary">Bei Metaplattform anmelden</a>
        </div>
    </div>
@endsection
