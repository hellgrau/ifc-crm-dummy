@extends('layouts.app')

@section('content')
    <div class="row mt-5">
        <div class="col-2 offset-5">
            <h2>Dummy-API</h2>

            <form action="{{ url('dummy-api/login') }}" method="POST">
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="email" class="form-control" id="email" name="email" autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="password">Passwort</label>
                    <input type="password" class="form-control" id="password" name="password" autocomplete="off">
                </div>

                <button class="btn btn-primary btn-block">Anmelden</button>
            </form>
        </div>
    </div>
@endsection
