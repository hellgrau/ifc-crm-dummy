<!DOCTYPE html>
<html lang="de">
<head>
    <title>Dummy-API</title>
    <style type="text/css">
        {{ file_get_contents(__CSS__.'/bootstrap.min.css') }}
    </style>
</head>
<body>
    @yield('content')
</body>
</html>
