<?php

return [
    'notification' => [
        'ttl' => env('EMBARGO_TTL', 86400),
        'subject' => env('EMBARGO_SUBJECT', 'Ein Embargo wurde bemerkt'),
        'sender' => [
            'email' => env('EMBARGO_SENDER_EMAIL'),
            'name' => env('EMBARGO_SENDER_NAME', 'Metaplattform')
        ],
        'receiver' => [
            'email' => env('EMBARGO_RECEIVER_EMAIL'),
            'name' => env('EMBARGO_RECEIVER_NAME'),
        ]
    ]
];
