<?php

// Angemeldet

use Hellgrau\DummyApi\Http\Middleware\DummySessionAuthMiddleware;

Route::group(['middleware' => DummySessionAuthMiddleware::class], function () {
    Route::get('', ['as' => 'start', 'uses' => 'StartController@index']);

    Route::get('sso/token', ['as' => 'sso.token', 'uses' => 'SsoController@token']);

    Route::get('logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);
});


Route::get('login', ['as' => 'login.index', 'uses' => 'LoginController@index']);
Route::post('login', ['as' => 'login', 'uses' => 'LoginController@login']);

