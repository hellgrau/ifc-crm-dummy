<?php

use Hellgrau\DummyApi\Http\Middleware\DummyApiAuthMiddleware;

// Angemeldet

Route::group(['middleware' => DummyApiAuthMiddleware::class], function () {
    Route::get('ldap', ['as' => 'ldap.index', 'uses' => 'LdapController@index']);
    Route::get('customers', ['as' => 'clients.index', 'uses' => 'CustomerController@index']);
    Route::get('customers/{id}', ['as' => 'clients.get', 'uses' => 'CustomerController@get'])->where('id', '[0-9]+');
    Route::get('customers/alpha-search', ['as' => 'clients.alpha-search', 'uses' => 'CustomerController@alphaSearch']);

    Route::post('documents', 'OrderDocumentController@store');
    Route::put('documents', 'OrderDocumentController@update');
    Route::delete('documents', 'OrderDocumentController@destroy');
});

