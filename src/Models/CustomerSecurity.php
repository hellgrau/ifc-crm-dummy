<?php

namespace Hellgrau\DummyApi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class CustomerSecurity extends Model
{
    protected $table = 'dummy_customer_securities';

    /**
     * @return HasOne
     */
    public function customer()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }
}
