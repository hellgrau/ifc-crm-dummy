<?php

namespace Hellgrau\DummyApi\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Customer
 * @package Hellgrau\DummyApi\App
 * @property string $salutation
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property Carbon $birthday
 * @property string $iban
 * @property string $bic
 * @property string $schufa_score
 * @property bool $is_applicant
 * @property bool $embargo
 * @property string $post_code
 * @property string $city
 * @property string $street
 * @property string $house_number
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 */
class Customer extends Model
{
    protected $table = 'dummy_customers';

    protected $dates = ['birthday', 'created_at', 'updated_at'];

    protected $casts = [
        'is_applicant' => 'bool',
        'embargo' => 'bool',
    ];

    /**
     * @return HasMany
     */
    public function securities()
    {
        return $this->hasMany(CustomerSecurity::class, 'customer_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function financialAssets()
    {
        return $this->hasMany(CustomerFinancialAsset::class, 'customer_id', 'id');
    }


    /**
     * @param $query
     * @param string $firstName
     * @param string $lastName
     * @param Carbon $birthday
     * @return mixed
     */
    public function scopeAlphaSearch($query, $firstName, $lastName, $birthday)
    {
        $query->where('first_name', $firstName);
        $query->where('last_name', $lastName);
        $query->whereDate('birthday', $birthday);

        return $query;
    }
}
