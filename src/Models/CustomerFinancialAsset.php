<?php

namespace Hellgrau\DummyApi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class CustomerFinancialAsset extends Model
{
    protected $table = 'dummy_customer_financial_assets';

    /**
     * @return HasOne
     */
    public function customer()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }
}
