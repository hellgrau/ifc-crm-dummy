<?php

namespace Hellgrau\DummyApi\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Embargo
 * @package Hellgrau\DummyApi\App
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property Carbon $birthday
 * @property string $nationality
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Embargo extends Model
{
    protected $table = 'dummy_embargoes';

    protected $dates = ['birthday', 'created_at', 'updated_at'];
}
