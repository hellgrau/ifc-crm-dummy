<?php

namespace Hellgrau\DummyApi\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Document
 * @package Hellgrau\DummyApi\App
 * @property int $id
 * @property int $order_id
 * @property int $category_id
 * @property int $offer_id
 * @property int $user_id
 * @property int $template_id
 * @property int $document_type_id
 * @property int $mandator_id
 * @property int $size
 * @property int $sort
 * @property string $mime
 * @property string $name
 * @property string $path
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 */
class Document extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $table = 'dummy_documents';
    protected $fillable = [
        'id',
        'order_id',
        'category_id',
        'offer_id',
        'user_id',
        'template_id',
        'document_type_id',
        'mandator_id',
        'size',
        'sort',
        'mime',
        'name',
        'path',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
