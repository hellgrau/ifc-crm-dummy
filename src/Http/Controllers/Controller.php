<?php

namespace Hellgrau\DummyApi\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        if (!defined('__CSS__')) {
            define('__CSS__', realpath(__DIR__ . '/../../../resources/css'));
        }


        $viewPath = realpath(__DIR__ . '/../../../resources/views');
        View::addLocation($viewPath);
    }
}
