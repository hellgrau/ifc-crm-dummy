<?php

namespace Hellgrau\DummyApi\Http\Controllers;

use Hellgrau\DummyApi\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        /** @var User $user */
        $user = User::query()->where('email', $request->email)->first();

        if ($user === null) {
            return redirect()->route('dummy-api.login.index');
        }

        if (!Hash::check($request->password, $user->password)) {
            return redirect()->route('dummy-api.login.index');
        }

        auth()->login($user);

        return redirect()->route('dummy-api.start');
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('dummy-api.login.index');
    }

    public function index()
    {
        return view('login');
    }
}
