<?php

namespace Hellgrau\DummyApi\Http\Controllers;

use Exception;
use GuzzleHttp\Client;

class SsoController extends Controller
{
    public function token()
    {
        $mpUrl = env('METAPLATTFORM_URL');
        $mpClientUrl = env('METAPLATTFORM_CLIENT_URL');
        $mpClientId = env('METAPLATTFORM_CLIENT_ID');
        $mpClientSecret = env('METAPLATTFORM_CLIENT_SECRET');

        try {
            $client = new Client([
                'auth' => [$mpClientId, $mpClientSecret]
            ]);

            $response = $client->post($mpClientUrl . '/token', ['form_params' => ['email' => auth()->user()->mp_email]]);

            $token = $response->getBody();

            if (!empty($token)) {
                return redirect()->to($mpUrl . '/login?token=' . $token);
            }
        } catch (Exception $e) {

        }
        return redirect()->route('dummy-api.start')->withErrors('SSO ist fehlgeschlagen');
    }

    public function index()
    {

    }
}
