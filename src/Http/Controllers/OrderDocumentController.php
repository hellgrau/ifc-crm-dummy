<?php

namespace Hellgrau\DummyApi\Http\Controllers;

use Hellgrau\DummyApi\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OrderDocumentController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|numeric|min:0|unique:dummy_documents',
            'file' => 'required|string'
        ]);

        $document = new Document();
        $document->fill($request->toArray());
        $document->save(['timestamps' => false]);

        Storage::put($document->path, base64_decode($request->get('file')));

        return response(['success' => true]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|numeric|min:0|exists:dummy_documents'
        ]);

        $document = Document::findOrFail($request->get('id'));
        $document->fill($request->toArray());
        $document->save(['timestamps' => false]);

        return response(['success' => true]);
    }

    public function destroy(Request $request)
    {
        if (!Document::find($request->get('id'))) {
            return response(['success' => true]);
        }

        return $this->update($request);
    }
}
