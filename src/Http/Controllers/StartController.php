<?php

namespace Hellgrau\DummyApi\Http\Controllers;

class StartController extends Controller
{
    public function index()
    {
        return view('start');
    }
}
