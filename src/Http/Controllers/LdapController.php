<?php

namespace Hellgrau\DummyApi\Http\Controllers;

/**
 * Dummy-API für das LDAP-System
 * Class LdapController
 * @package Hellgrau\DummyApi\Http\Controllers
 */
class LdapController extends Controller
{

    /**
     * Dummy-Route für das LDAP-System. Eine User-ID bekommt Rechte zugeordnet
     * @return array
     */
    public function index()
    {
        return response()->json([
            'users' => [
                [
                    'id' => 'admin-via',
                    'role' => 'ADM',
                    'group' => 2

                ],
                [
                    'id' => 'moderator-via',
                    'role' => 'MOD',
                    'group' => 2
                ],
                [
                    'id' => 'bearbeiter-via',
                    'role' => 'BEA',
                    'group' => 2
                ],
                [
                    'id' => 'admin-spb',
                    'role' => 'ADM',
                    'group' => 4

                ],
                [
                    'id' => 'moderator-spb',
                    'role' => 'MOD',
                    'group' => 4
                ],
                [
                    'id' => 'bearbeiter-spb',
                    'role' => 'BEA',
                    'group' => 4
                ]
            ],
            'roles' => [
                'ADM',
                'MOD',
                'BEA'
            ],
            'groups' => [
                ['id' => 1, 'name' => 'Viantis', 'parent_id' => null],
                ['id' => 2, 'name' => 'Düsseldorf', 'parent_id' => 1],
                ['id' => 3, 'name' => 'Sparda-Bank', 'parent_id' => null],
                ['id' => 4, 'name' => 'West', 'parent_id' => 3],
            ]
        ], 200, [], JSON_PRETTY_PRINT);
    }
}
