<?php

namespace Hellgrau\DummyApi\Http\Controllers;

use Carbon\Carbon;
use Hellgrau\DummyApi\Models\Customer;
use Hellgrau\DummyApi\Services\EmbargoService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CustomerController extends Controller
{
    /**
     * @param $clientId
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function get($clientId)
    {
        return Customer::query()->with(['financialAssets', 'securities'])->findOrFail($clientId);
    }

    /**
     * @param Request $request
     * @throws ValidationException
     * @return array
     */
    public function alphaSearch(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'birthday' => 'required|date',
        ]);

        $firstName = $request->get('first_name', null);
        $lastName = $request->get('last_name', null);
        $birthday = Carbon::parse($request->get('birthday', null));

        $customers = Customer::query()
            ->with(['financialAssets', 'securities'])
            ->alphaSearch($firstName, $lastName, $birthday)
            ->get();

        return [
            'doublets' => $customers,
            'embargo' => EmbargoService::check($firstName, $lastName, $birthday)
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $customers = Customer::query()
            ->with(['financialAssets', 'securities'])
            ->get();

        return $customers;
    }
}
