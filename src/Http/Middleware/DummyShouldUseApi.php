<?php

namespace Hellgrau\DummyApi\Http\Middleware;

use Closure;

class DummyShouldUseApi
{
    public function handle($request, Closure $next, $guard = null)
    {
        auth()->shouldUse('dummy-api');

        return $next($request);
    }
}
