<?php

namespace Hellgrau\DummyApi\Http\Middleware;

use Closure;

class DummyShouldUseSession
{
    public function handle($request, Closure $next, $guard = null)
    {
        auth()->shouldUse('dummy-session');

        return $next($request);
    }
}
