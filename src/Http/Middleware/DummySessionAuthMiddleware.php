<?php

namespace Hellgrau\DummyApi\Http\Middleware;

use Closure;

class DummySessionAuthMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (!auth()->check()) {
            return redirect()->route('dummy-api.login.index');
        }

        return $next($request);
    }
}
