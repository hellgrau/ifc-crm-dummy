<?php

namespace Hellgrau\DummyApi\Http\Middleware;

use Closure;

class DummyApiAuthMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (!auth()->check()) {
            return response(['error' => 'invalid credentials'], 401);
        }

        return $next($request);
    }
}
