<?php

namespace Hellgrau\DummyApi\Extensions;

use Hellgrau\DummyApi\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class DummySessionGuard
 * @package Hellgrau\DummyApi\Extensions
 * @property User $user
 */
class DummySessionGuard implements Guard
{

    protected $user;
    protected $sessionKey = 'dummy_user_id';

    /**
     * Determine if the current user is authenticated.
     * @return bool
     */
    public function check()
    {
        return $this->user() !== null;
    }

    /**
     * Get the currently authenticated user.
     * @return Authenticatable|null
     */
    public function user()
    {
        $userId = session()->get('dummy_user_id', null);

        if ($userId !== null) {
            $this->user = User::query()->find($userId);
        }

        return $this->user;
    }

    /**
     * Determine if the current user is a guest.
     * @return bool
     */
    public function guest()
    {
        return $this->user() === null;
    }

    /**
     * Get the ID for the currently authenticated user.
     * @return int|string|null
     */
    public function id()
    {
        $this->user->getAuthIdentifier();
    }

    /**
     * Validate a user's credentials.
     * @param array $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        // TODO: Implement validate() method.
    }

    public function logout()
    {
        session()->forget($this->sessionKey);
    }

    public function login(Authenticatable $user)
    {
        session()->put($this->sessionKey, $user->getAuthIdentifier());
        $this->setUser($user);
    }

    /**
     * Set the current user.
     * @param Authenticatable $user
     * @return void
     */
    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
    }
}
