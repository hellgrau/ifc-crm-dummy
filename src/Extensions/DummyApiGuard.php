<?php

namespace Hellgrau\DummyApi\Extensions;

use Hellgrau\DummyApi\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class DummySessionGuard
 * @package Hellgrau\DummyApi\Extensions
 * @property User $user
 */
class DummyApiGuard implements Guard
{

    protected $user;
    protected $request;

    public function __construct()
    {
        $this->request = request();
    }


    /**
     * Determine if the current user is authenticated.
     * @return bool
     */
    public function check()
    {
        $credentials = ['secret' => env('DUMMY_API_SECRET')];

        return $this->validate($credentials) === true;
    }

    /**
     * Validate a user's credentials.
     * @param array $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {

        if ($this->request->bearerToken() === $credentials['secret']) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the current user is a guest.
     * @return bool
     */
    public function guest()
    {
        $credentials = ['secret' => env('DUMMY_API_SECRET')];

        return $this->validate($credentials) === false;
    }

    /**
     * Get the currently authenticated user.
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function user()
    {
    }

    /**
     * Get the ID for the currently authenticated user.
     * @return int|string|null
     */
    public function id()
    {
    }

    /**
     * Set the current user.
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @return void
     */
    public function setUser(Authenticatable $user)
    {
        return;
    }
}
