<?php

namespace Hellgrau\DummyApi;

use Hellgrau\DummyApi\Extensions\DummyApiGuard;
use Hellgrau\DummyApi\Extensions\DummySessionGuard;
use Hellgrau\DummyApi\Http\Middleware\DummyShouldUseApi;
use Hellgrau\DummyApi\Http\Middleware\DummyShouldUseSession;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        $this->loadMigrationsFrom(realpath(__DIR__ . '/../database/migrations'));
        $this->registerEloquentFactoriesFrom(realpath(__DIR__ . '/../database/factories'));
        $this->loadViewsFrom(realpath(__DIR__ . '/../resources/views'), 'dummy');
        $this->mergeConfigFrom(__DIR__ . '/../config/embargo.php', 'embargo');

        $this->registerWebRoutes();
        $this->registerApiRoutes();
    }

    protected function registerEloquentFactoriesFrom($path)
    {
        $this->app->make(Factory::class)->load($path);
    }

    protected function registerWebRoutes()
    {
        Auth::extend('dummy-session', function ($app, $name, array $config) {
            return new DummySessionGuard();
        });

        config()->set('auth.guards.dummy-session', ['driver' => 'dummy-session']);

        Route::prefix('dummy-api')
            ->as('dummy-api.')
            ->namespace('Hellgrau\DummyApi\Http\Controllers')
            ->middleware([StartSession::class, DummyShouldUseSession::class])
            ->group(realpath(__DIR__ . '/../routes/web.php'));
    }

    protected function registerApiRoutes()
    {
        Auth::extend('dummy-api', function ($app, $name, array $config) {
            return new DummyApiGuard();
        });

        config()->set('auth.guards.dummy-api', ['driver' => 'dummy-api']);

        Route::prefix('dummy-api')
            ->as('dummy-api.')
            ->namespace('Hellgrau\DummyApi\Http\Controllers')
            ->middleware([DummyShouldUseApi::class])
            ->group(realpath(__DIR__ . '/../routes/api.php'));
    }
}
