<?php

namespace Hellgrau\DummyApi\Services;

use Carbon\Carbon;
use Hellgrau\DummyApi\Models\Embargo;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;

class EmbargoService
{
    protected $firstName;
    protected $lastName;
    protected $birthday;
    protected $config;

    public function __construct($firstName, $lastName, Carbon $birthday)
    {
        $this->config = config('embargo.notification');
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->birthday = $birthday;
    }

    /**
     * @param $firstName
     * @param $lastName
     * @param Carbon $birthday
     * @return array|bool
     */
    public static function check($firstName, $lastName, Carbon $birthday)
    {
        $self = new self($firstName, $lastName, $birthday);
        return $self->getList();
    }


    /**
     * @return array|bool
     */
    public function getList()
    {
        $embargo = Embargo::query()
            ->where('first_name', $this->firstName)
            ->where('last_name', $this->lastName)
            ->whereDate('birthday', $this->birthday->toDateString())
            ->first();

        if (!$embargo) {
            return false;
        }

        $this->sendMailNotification();

        return $embargo->toArray();
    }


    public function sendMailNotification()
    {
        $hash = md5($this->firstName . $this->lastName . $this->birthday->toDateString());

        Cache::remember($hash, $this->config['ttl'], function () {
            Mail::send('dummy::emails.embargo', [
                'firstName' => $this->firstName,
                'lastName' => $this->lastName,
                'birthday' => $this->birthday
            ], function ($message) {
                $message->from($this->config['sender']['email'], $this->config['sender']['name']);
                $message->to($this->config['receiver']['email'], $this->config['receiver']['name']);
                $message->subject($this->config['subject']);
            });

            return true;
        });
    }
}
